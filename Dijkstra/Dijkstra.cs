﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dijkstra
{
    /// <summary>
    /// Алгоритм Дейкстри
    /// </summary>
    public class Dijkstra
    {
        Graph graph; //поле для графа з вершинами

        List<GraphVertexInfo> infos; //List з інформацією про кожне поле

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="graph">Граф</param>
        public Dijkstra(Graph graph)
        {
            this.graph = graph;
        }

        /// <summary>
        /// Додавання до списку новостворену інформацію про вершини
        /// </summary>
        void InitInfo()
        {
            infos = new List<GraphVertexInfo>();
            foreach (var v in graph.Vertices) //перебирання всіх вершин графу
            {
                infos.Add(new GraphVertexInfo(v)); //додавання до списку початкової інформації про вершину
            }
        }

        /// <summary>
        /// Отримання інформації про вершину графа
        /// </summary>
        /// <param name="v">Об'єкт класу вершини</param>
        /// <returns>Інформація про вершину</returns>
        GraphVertexInfo GetVertexInfo(GraphVertex v)
        {
            foreach (var i in infos) //перебирання списку з інформацією про вершини
            {
                if (i.Vertex.Equals(v)) //якщо вершина сходиться з тією, що шукаємо
                {
                    return i; //повертаємо інформацію про цю вершину
                }
            }

            return null;
        }

        /// <summary>
        /// Пошук невідвіданої вершини з мінімальною сумою шляху, який ми подолали щоб до неї дістатися
        /// </summary>
        /// <returns>Інформація про вершину</returns>
        public GraphVertexInfo FindUnvisitedVertexWithMinSum()
        {
            var minValue = int.MaxValue;
            GraphVertexInfo minVertexInfo = null;
            foreach (var i in infos) //перебирання списку з інформацією про вершини
            {
                if (i.IsUnvisited && i.EdgesWeightSum < minValue) //якщо вершина не відвідана і сума шляхів менша за мінімальне значення
                {
                    minVertexInfo = i; //інформація про вершину з мінімальним шляхом
                    minValue = i.EdgesWeightSum; //мінімальне значення шляху дорівнює шляху цієї вершини
                }
            }

            return minVertexInfo; //повернення вершини з мінімальною дистанцією
        }

        /// <summary>
        /// Знаходження довжини шляху до наступної вершини
        /// </summary>
        /// <param name="info">Інформація про активну вершину</param>
        void SetSumToNextVertex(GraphVertexInfo info)
        {
            info.IsUnvisited = false; //позначка, що дана вершина була відвідана
            foreach (var e in info.Vertex.Edges) //перебирання всіх ребер активної вершини
            {
                var nextInfo = GetVertexInfo(e.ConnectedVertex); //інформація про наступну вершину
                var sum = info.EdgesWeightSum + e.EdgeWeight; //шлях = довжина шляху до активної вершини + довжина шляху до прив'язаної вершини
                if (sum < nextInfo.EdgesWeightSum) //якщо шлях менший за шлях до наступної вершини
                {
                    nextInfo.EdgesWeightSum = sum; //шлях до наступної вершини дорівнює шляху, що вже пройшли
                    nextInfo.PreviousVertex = info.Vertex; //попередня вершина для наступної стає активна вершина
                }
            }
        }

        /// <summary>
        /// Формування шляху
        /// </summary>
        /// <param name="startVertex">Стартова вершина</param>
        /// <param name="endVertex">Вершина до якої необхідно дібратися</param>
        /// <returns>Шлях</returns>
        string GetPath(GraphVertex startVertex, GraphVertex endVertex)
            {
            GraphVertexInfo info = GetVertexInfo(endVertex);
            var path = endVertex.ToString() + " --> " + info.EdgesWeightSum; //ім'я кінцевої вершини
            while (startVertex != endVertex) //поки початкова вершина не дорівнює кінцевій вершині
            {
                endVertex = GetVertexInfo(endVertex).PreviousVertex; //перехід до попередньої вериши кінцевої вершини
                path = endVertex.ToString()+ " --> " + path; //додавання до шляху назв вершин, які були попередніми одна у одної
            }

            return path; //повернення шляху
        }

        /// <summary>
        /// Пошук найкоротшого шляху по вершинах
        /// </summary>
        /// <param name="startVertex">Початкова вершина</param>
        /// <param name="finishVertex">Вершина, до якої необхідно дістатися</param>
        /// <returns>Найкоротший шлях</returns>
        public string FindShortestPath(GraphVertex startVertex, GraphVertex finishVertex)
        {
            InitInfo(); //створення списку з початковою інформацією про вершини
            var first = GetVertexInfo(startVertex); // знаходження інформації про стартову вершину
            first.EdgesWeightSum = 0; //довжина шляху до стартової вершини дорівнює нулю
            while (true)
            {
                var current = FindUnvisitedVertexWithMinSum(); //встановлення стартової першини, як вершини з мінімальним значенням шляху
                if (current == null) //якщо такої вершини не існує, то наступна ітерація
                {
                    break;
                }

                SetSumToNextVertex(current); //перехід до наступної вершини з найкоротшим шляхом від активної
            }

            return GetPath(startVertex, finishVertex); //шлях від початкової до кінцевої
        }

        /// <summary>
        /// Пошук найкоротшого шляху по назвах вершин
        /// </summary>
        /// <param name="startName">Назва початкової вериши</param>
        /// <param name="finishName">Назва вершини, до якої необхідно дійти</param>
        /// <returns>Найкоротший шлях</returns>
        public string FindShortestPath(string startName, string finishName)
        {
            return FindShortestPath(graph.FindVertex(startName), graph.FindVertex(finishName));
        }

        

        
    }
}
