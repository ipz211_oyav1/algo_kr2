﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dijkstra
{
    /// <summary>
    /// Інформація про вершину
    /// </summary>
    public class GraphVertexInfo
    {
        /// <summary>
        /// Вершина
        /// </summary>
        public GraphVertex Vertex { get; set; }

        /// <summary>
        /// Чи відвідувалася вершина
        /// </summary>
        public bool IsUnvisited { get; set; }

        /// <summary>
        /// Сума ваги ребер, які ми пройшли щоб дістатися до цієї вершини
        /// </summary>
        public int EdgesWeightSum { get; set; }

        /// <summary>
        /// Вершина з якої ми перейшли до цієї
        /// </summary>
        public GraphVertex PreviousVertex { get; set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="vertex">Вершина</param>
        public GraphVertexInfo(GraphVertex vertex)
        {
            Vertex = vertex;
            IsUnvisited = true;
            EdgesWeightSum = int.MaxValue;
            PreviousVertex = null;
        }
    }
}
