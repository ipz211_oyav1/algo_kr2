﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Dijkstra
{
    /// <summary>
    /// Граф
    /// </summary>
    public class Graph
    {
        /// <summary>
        /// Список вершин графа
        /// </summary>
        public List<GraphVertex> Vertices { get; } //поле типу List, що буде складатися з вершин

        /// <summary>
        /// Конструктор створення пустого списку, що матиме інформацію про вершини
        /// </summary>
        public Graph()
        {
            Vertices = new List<GraphVertex>();
        }

        /// <summary>
        /// Додавання вершини
        /// </summary>
        /// <param name="vertexName">Ім'я вершини</param>
        public void AddVertex(string vertexName)
        {
            Vertices.Add(new GraphVertex(vertexName));//створює нову вершину та пустий список прив'язаних до цієї вершини інших вершин
        }

        /// <summary>
        /// Пошук вершини
        /// </summary>
        /// <param name="vertexName">Назва вершини</param>
        /// <returns>Знайдена вершина</returns>
        public GraphVertex FindVertex(string vertexName)
        {
            foreach (var v in Vertices) //перебирання списку вершин
            {
                if (v.Name.Equals(vertexName)) //пошук відповідності назви вершини
                {
                    return v;
                }
            }

            return null;
        }

        /// <summary>
        /// Додавання ребра
        /// </summary>
        /// <param name="firstName">Ім'я першої вершини</param>
        /// <param name="secondName">Ім'я другої вершини</param>
        /// <param name="weight">Вага ребра, яке з'єднує ці вершини</param>
        public void AddEdge(string firstName, string secondName, int weight)
        {
            var v1 = FindVertex(firstName);
            var v2 = FindVertex(secondName);
            if (v2 != null && v1 != null)//перевірка на існування вершин, які намагаємось створити
            {
                v1.AddEdge(v2, weight);
                v2.AddEdge(v1, weight);
            }
        }
    }

    

    


}
