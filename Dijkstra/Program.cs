﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dijkstra
{
    
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.InputEncoding = System.Text.Encoding.UTF8;
            System.Console.InputEncoding = Encoding.GetEncoding(1251);

            var g = new Graph();

            g.AddVertex("Київ");
            g.AddVertex("Житомир");
            g.AddVertex("Новоград-Волинський");
            g.AddVertex("Рівно");
            g.AddVertex("Луцьк");
            g.AddVertex("Вінниця");
            g.AddVertex("Бердичів");
            g.AddVertex("Хмельницький");
            g.AddVertex("Тернопіль");
            g.AddVertex("Шепетівка");
            g.AddVertex("Біла Церква");
            g.AddVertex("Умань");
            g.AddVertex("Черкаси");
            g.AddVertex("Кременчуг");
            g.AddVertex("Полтава");
            g.AddVertex("Харків");
            g.AddVertex("Прилуки");
            g.AddVertex("Суми");
            g.AddVertex("Миргород");

            
            g.AddEdge("Київ", "Житомир", 135);
            g.AddEdge("Житомир", "Новоград-Волинський", 80);
            g.AddEdge("Новоград-Волинський", "Рівно", 100);
            g.AddEdge("Рівно", "Луцьк", 68);
            g.AddEdge("Житомир", "Бердичів", 38);
            g.AddEdge("Бердичів", "Вінниця", 73);
            g.AddEdge("Вінниця", "Хмельницький", 110);
            g.AddEdge("Хмельницький", "Тернопіль", 104);
            g.AddEdge("Житомир", "Шепетівка", 115);
            g.AddEdge("Київ", "Біла Церква", 78);
            g.AddEdge("Біла Церква", "Умань", 115);
            g.AddEdge("Біла Церква", "Черкаси", 146);
            g.AddEdge("Черкаси", "Кременчуг", 105);
            g.AddEdge("Біла Церква", "Полтава", 181);
            g.AddEdge("Полтава", "Харків", 130);
            g.AddEdge("Київ", "Прилуки", 128);
            g.AddEdge("Прилуки", "Суми", 175);
            g.AddEdge("Прилуки", "Миргород", 109);
            g.AddEdge("Шепетівка", "Бердичів", 122);
            g.AddEdge("Шепетівка", "Вінниця", 180);
            g.AddEdge("Шепетівка", "Хмельницький", 100);          
            g.AddEdge("Шепетівка", "Тернопіль", 160);
            g.AddEdge("Шепетівка", "Новоград-Волинський", 70);
            g.AddEdge("Шепетівка", "Рівно", 94);
            g.AddEdge("Шепетівка", "Луцьк", 169);
            g.AddEdge("Тернопіль", "Луцьк", 168);
            g.AddEdge("Бердичів", "Біла Церква", 133);
            g.AddEdge("Бердичів", "Умань", 247);
            g.AddEdge("Вінниця", "Умань", 161);
            g.AddEdge("Черкаси", "Умань", 184);
            g.AddEdge("Кременчуг", "Умань", 308);
            g.AddEdge("Черкаси", "Полтава", 241);
            g.AddEdge("Кременчуг", "Полтава", 113);
            g.AddEdge("Біла Церква", "Прилуки", 277);
            g.AddEdge("Біла Церква", "Миргород", 356);
            g.AddEdge("Суми", "Миргород", 180);
            g.AddEdge("Полтава", "Миргород", 108);
            g.AddEdge("Суми", "Полтава", 176);
            g.AddEdge("Суми", "Харків", 186);

            var dijkstra = new Dijkstra(g);
            var path = dijkstra.FindShortestPath("Умань", "Рівно");
            Console.WriteLine(path);
            Console.ReadLine();
        }
    }
}
