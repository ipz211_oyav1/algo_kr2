﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dijkstra
{
    /// <summary>
    /// Вершина графа
    /// </summary>
    public class GraphVertex
    {
        /// <summary>
        /// Назва вершини
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Список ребер(мітить верниши, які зв'язані з цією своїми ребрами)
        /// </summary>
        public List<GraphEdge> Edges { get; } //створюється пустий список прив'язаних вершин до цієї

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="vertexName">Назва вершини</param>
        public GraphVertex(string vertexName)
        {
            Name = vertexName; //заповнення поля Name;
            Edges = new List<GraphEdge>(); //створення пустого списку прив'язаних вершин
        }

        /// <summary>
        /// Додавання до списку Edges нового ребра
        /// </summary>
        /// <param name="newEdge">Ребро</param>
        public void AddEdge(GraphEdge newEdge)
        {
            Edges.Add(newEdge); //додавання до списку нового ребра
        }

        /// <summary>
        /// Додавання ребра, яке прив'язане до цієї вершини
        /// </summary>
        /// <param name="vertex">Вершина</param>
        /// <param name="edgeWeight">Вага</param>
        public void AddEdge(GraphVertex vertex, int edgeWeight)
        {
            AddEdge(new GraphEdge(vertex, edgeWeight));//додавання до списку ребра зв'язаного з цією вершиною
        }

        /// <summary>
        /// Повернення значення імені вершини
        /// </summary>
        /// <returns>Ім'я вершини</returns>
        public override string ToString() => Name;
    }
}
