﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dijkstra
{
    /// <summary>
    /// Ребро графа (містить ребро та одну прив'язану до нього вершину)
    /// </summary>
    public class GraphEdge
    {
        /// <summary>
        /// Вершина, яка прив'язана до ребра
        /// </summary>
        public GraphVertex ConnectedVertex { get; }

        /// <summary>
        /// Вага ребра
        /// </summary>
        public int EdgeWeight { get; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="connectedVertex">Верниша, яка прив'язана до ребра</param>
        /// <param name="weight">Вага ребра</param>
        public GraphEdge(GraphVertex connectedVertex, int weight)
        {
            ConnectedVertex = connectedVertex;
            EdgeWeight = weight;
        }
    }
}
